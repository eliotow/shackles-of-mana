﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spellRecipes : MonoBehaviour {

    public GameObject fireball;

    public GameObject gust;

    public float spellSpeed;



	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (RightPointer_Control.rightPointerUp == true && RightMiddle_Control.rightMiddleUp == false && RightRing_Control.rightRingUp == false && RightPinky_Control.rightPinkyUp == true && Input.GetKeyDown (KeyCode.Space))
			castFireball();
        if (RightPointer_Control.rightPointerUp == true && RightMiddle_Control.rightMiddleUp == true && RightRing_Control.rightRingUp == true && RightPinky_Control.rightPinkyUp == false && Input.GetKeyDown(KeyCode.Space))
            castGust();
        if (Input.GetKeyDown(KeyCode.Space))
            castFireball();

    }

	void castFireball()
    {
        //GameObject newFireball = Instantiate(fireball, GameObject.Find("Player").transform.position, transform.rotation);
        //newFireball.GetComponent<Rigidbody2D>().AddForce(GameObject.Find("Player").GetComponent<Player>().forwardDirection * spellSpeed * Time.deltaTime);

        GameObject newFireball = Instantiate(fireball, GameObject.Find("Player").transform.position, transform.rotation);
        //newFireball.GetComponent<Rigidbody>().AddForce(GameObject.Find("Player").GetComponent<Player>().forwardDirection * spellSpeed * Time.deltaTime);

    }

    void castGust()
    {
        GameObject newGust = Instantiate(gust, GameObject.Find("Player").transform.position, transform.rotation);
        newGust.GetComponent<Rigidbody2D>().AddForce(GameObject.Find("Player").GetComponent<Player>().forwardDirection * spellSpeed * Time.deltaTime);

    }
		
}
