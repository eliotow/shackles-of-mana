﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform playerPos;

    public float cameraDistance = 30.0f;

    void Awake()
    {
        GetComponent<UnityEngine.Camera> ().orthographicSize = ((Screen.height / 2) / cameraDistance);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {

        transform.position = new Vector3(playerPos.position.x, /*playerPos.position.y*/ 40 , playerPos.position.z);

    }
}
