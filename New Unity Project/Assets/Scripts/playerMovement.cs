﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour {
    public float speed;
    public bool W_down = false;
    public bool A_down = false;
    public bool S_down = false;
    public bool D_down = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.D)& W_down == false & A_down == false & S_down == false)
        {
            transform.Translate(Vector2.right * speed* Time.deltaTime);
            D_down = true;
        }

        if (Input.GetKey(KeyCode.A) & W_down == false & D_down == false & S_down == false)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
            A_down = true;
        }

        if (Input.GetKey(KeyCode.S) & W_down == false & A_down == false & D_down == false)
        {
            transform.Translate(Vector2.down * speed * Time.deltaTime);
            S_down = true;
        }

        if (Input.GetKey(KeyCode.W) & D_down == false & A_down == false & S_down == false)
        {
            transform.Translate(Vector2.up * speed* Time.deltaTime);
            W_down = true;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            D_down = false;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            A_down = false;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            W_down = false;
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            S_down = false;
        }
    }
}
