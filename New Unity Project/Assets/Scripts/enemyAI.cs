﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;


public class enemyAI : MonoBehaviour {
    public Transform targetPoint;
    public GameObject sightline;
    public bool isTriggered;
    private NavMeshAgent navComponent;

	// Use this for initialization
    void Start () {
        //NavMeshAgent agent = GetComponent<NavMeshAgent>();
        navComponent = this.transform.GetComponent<NavMeshAgent>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (isTriggered == true )
        {
            navComponent.SetDestination(targetPoint.position);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            isTriggered = true;
        }
    }


}
