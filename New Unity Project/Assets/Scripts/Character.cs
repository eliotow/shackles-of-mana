﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour {

    [SerializeField]
    private float speed;

    protected Vector3 direction;




    // Use this for initialization
    protected virtual void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update ()
    {

        Move();

    }

    public void Move()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }

}
