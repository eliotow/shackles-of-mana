﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character {


    public Vector3 forwardDirection;

    [SerializeField]
    private Stat health;

    [SerializeField]
    private Stat mana;


    [SerializeField]
    private float initHealth;

    [SerializeField]
    private float initMana;

   protected override void Start()
    {
        direction = Vector3.up;
        health.Initialize(initHealth, initHealth);     
        mana.Initialize(initMana, initMana);
        base.Start();
    }



    // Use this for initialization

    // Update is called once per frame
    protected override void Update() {

        GetInput();


        base.Update();

    }


    private void GetInput()
    {
        direction = Vector2.zero;

        //debug to check mana/health increments
        if (Input.GetKeyDown(KeyCode.M))
        {
            health.MyCurrentValue -= 10;
            mana.MyCurrentValue -= 10;
        }
       if (Input.GetKeyDown(KeyCode.N))
        {
            health.MyCurrentValue += 10;
            mana.MyCurrentValue += 10;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            forwardDirection = Vector3.forward;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            forwardDirection = Vector3.left;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            forwardDirection = Vector3.back;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            forwardDirection = Vector3.right;
        }



        if (Input.GetKey(KeyCode.W))
        {
            direction += Vector3.forward;

        }
        if (Input.GetKey(KeyCode.A))
        {
            direction += Vector3.left;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction += Vector3.back;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector3.right;
        }

    }


}
